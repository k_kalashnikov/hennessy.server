﻿window.delayedFunction = new Object;

function getOurHeightMax(elList) {
    var maxH = -1;
    $(elList).each(function () {
        var curH = parseInt($(this).height());
        maxH = (curH > maxH) ? curH : maxH;
    });
    $(elList).each(function () {
        $(this).height(maxH);
    });
}

function FormToggle(selector, elListSelect)
{
    $(selector).on('shown.bs.tab', function () {
        console.log('shown.bs.tab');
        console.log(this);
        getOurHeightMax(elListSelect);
    });
}


function getOurHeightMin(elList) {
    var maxH = $(elList).first().height();
    $(elList).each(function () {
        var curH = parseInt($(this).height());
        maxH = (curH < maxH) ? curH : maxH;
    });
    console.log(maxH);
    $(elList).each(function () {
        $(this).height(maxH);
    });
}

function TableObject(element)
{
    var self = this;
    self.Obj = $(element);

    self.Init = function ()
    {
        self.table = self.Obj.DataTable({
            stateSave: true,
            responsive: true,
            language: {
                "processing": "Подождите...",
                "search": "Поиск:",
                "lengthMenu": "Показать _MENU_ записей",
                "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
                "infoEmpty": "Записи с 0 до 0 из 0 записей",
                "infoFiltered": "(отфильтровано из _MAX_ записей)",
                "infoPostFix": "",
                "loadingRecords": "Загрузка записей...",
                "zeroRecords": "Записи отсутствуют.",
                "emptyTable:": "В таблице отсутствуют данные",
                "paginate": {
                    "first": "Первая",
                    "previous": "Предыдущая",
                    "next": "Следующая",
                    "last": "Последняя"
                },
                "aria": {
                    "sortAscending": ": активировать для сортировки столбца по возрастанию",
                    "sortDescending": ": активировать для сортировки столбца по убыванию"
                }
            }
        });

        self.Obj.find('.delete').click(function ()
        {
            var deleteLink = $(this);
            if (!confirm("Вы действительно хотите удалить элемент?")) {
                return false;
            }
            var dataPost = deleteLink.attr('data-post');
            var url = deleteLink.attr('href');
            var token = $('input[name="__RequestVerificationToken"]').val();
            var data =
            {
                id: dataPost,
                __RequestVerificationToken: token
            };
            $.post(url,
                data,
                function (result) {
                    if (result.result) {
                        deleteLink.closest('tr').css('background-color', '#ff5555');
                        deleteLink.parent().html('<span>УДАЛЁН</span>');
                    }
                    else
                    {
                        alert(result.message);
                    }
            });
            return false;
        });;
    }

    self.Init();
}

function TopMainMenuItem(element)
{
    var self = this;
    self.obj = $(element);
    self.mainBlock = self.obj.parent().parent();
    self.targetHref = self.obj.attr('href');

    self.Init = function ()
    {
        self.obj.click(function ()
        {
            if (self.obj.attr('data-drop-down') == "true")
            {
                self.mainBlock.hide();
                var target = $(self.targetHref);
                target.show();
                return false;
            }
        });
    }

    self.Debug = function ()
    {
        console.log(self.obj);
        console.log(self.mainBlock);
        console.log(self.targetHref);
    }

    self.Init();
}

function SelectItem(element)
{
    var self = this;
    self.Obj = $(element);
    self.Href = self.Obj.attr('data-href');

    self.Init = function ()
    {
        var selectObj = $('option[data-href="' + self.Href + '"]');
        console.log(selectObj);
        if (selectObj.attr('selected') == 'selected') {
            self.Obj.removeClass('list-group-item');
            self.Obj.addClass('list-group-item-success');
        }
    }

    self.Chouse = function ()
    {
        if (self.Obj.hasClass('list-group-item-success'))
        {
            self.Obj.removeClass('list-group-item-success');
            self.Obj.addClass('list-group-item');
            $('option[data-href="' + self.Href + '"]').removeAttr('selected');
            $('option[data-href="' + self.Href + '"]').prop('selected', false);
        }
        else
        {
            self.Obj.removeClass('list-group-item');
            self.Obj.addClass('list-group-item-success');
            $('option[data-href="' + self.Href + '"]').attr('selected', 'selected');
            $('option[data-href="' + self.Href + '"]').prop('selected', true);


        }
    }

    self.Obj.click(function () {
        self.Chouse();
    });

    self.Init();
}

function DinamicItem(element)
{
    var self = this;
    self.Obj = $(element);
    self.Href = self.Obj.attr('id');
    self.RemoveItem = function (el) { }

    self.Init = function (RemoveItem)
    {
        self.RemoveItem = RemoveItem;

        $('span[data-href="' + self.Href + '"]').unbind('click');

        $('span[data-href="' + self.Href + '"]').click(function () {
            self.RemoveItem(self.Obj);
            return false;
        });
    }
}

$(window).load(function () {
    $('.preloader').hide();
    for (var item in window.delayedFunction) {
        console.log(item + " comlited.");
        window.delayedFunction[item]();
    }
});
