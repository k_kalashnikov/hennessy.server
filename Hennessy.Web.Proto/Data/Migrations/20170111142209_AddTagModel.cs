﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Hennessy.Web.Proto.Data.Migrations
{
    public partial class AddTagModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Tags",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    LastUpdate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tags", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProductTag",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    LastUpdate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    ProductId = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    TagId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductTag", x => x.Id);
                    table.UniqueConstraint("AK_ProductTag_ProductId_TagId", x => new { x.ProductId, x.TagId });
                    table.ForeignKey(
                        name: "FK_ProductTag_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductTag_Tags_TagId",
                        column: x => x.TagId,
                        principalTable: "Tags",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Volumes",
                nullable: false);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "RecipeItems",
                nullable: false);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Recipes",
                nullable: false);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Properties",
                nullable: false);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "ProductVolumes",
                nullable: false);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "ProductRecipe",
                nullable: false);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "ProductAromatics",
                nullable: false);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Products",
                nullable: false);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Images",
                nullable: false);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Brands",
                nullable: false);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Aromatics",
                nullable: false);

            migrationBuilder.CreateIndex(
                name: "IX_ProductTag_ProductId",
                table: "ProductTag",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductTag_TagId",
                table: "ProductTag",
                column: "TagId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProductTag");

            migrationBuilder.DropTable(
                name: "Tags");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Volumes",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "RecipeItems",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Recipes",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Properties",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "ProductVolumes",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "ProductRecipe",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "ProductAromatics",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Products",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Images",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Brands",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Aromatics",
                nullable: true);
        }
    }
}
