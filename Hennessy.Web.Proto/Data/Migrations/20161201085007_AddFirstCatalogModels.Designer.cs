﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Hennessy.Core.Proto.Models;

namespace Hennessy.Web.Proto.Data.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20161201085007_AddFirstCatalogModels")]
    partial class AddFirstCatalogModels
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Hennessy.Core.Proto.Models.ApplicationUser", b =>
                {
                    b.Property<string>("Id");

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedUserName")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasAnnotation("MaxLength", 256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("Hennessy.Core.Proto.Models.Aromatic", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("LastUpdate");

                    b.Property<string>("Name");

                    b.Property<int>("Status");

                    b.HasKey("Id");

                    b.ToTable("Aromatics");
                });

            modelBuilder.Entity("Hennessy.Core.Proto.Models.Brand", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description");

                    b.Property<DateTime>("LastUpdate");

                    b.Property<string>("Name");

                    b.Property<int>("Status");

                    b.HasKey("Id");

                    b.ToTable("Brands");
                });

            modelBuilder.Entity("Hennessy.Core.Proto.Models.Image", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("AromaticId");

                    b.Property<int?>("BrandId");

                    b.Property<string>("Checksum");

                    b.Property<DateTime>("LastUpdate");

                    b.Property<string>("Name");

                    b.Property<int?>("ProductId");

                    b.Property<int?>("RecipeId");

                    b.Property<int>("Status");

                    b.HasKey("Id");

                    b.HasIndex("AromaticId")
                        .IsUnique();

                    b.HasIndex("BrandId");

                    b.HasIndex("ProductId");

                    b.HasIndex("RecipeId")
                        .IsUnique();

                    b.ToTable("Images");
                });

            modelBuilder.Entity("Hennessy.Core.Proto.Models.Product", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("BrandId");

                    b.Property<string>("Description");

                    b.Property<DateTime>("LastUpdate");

                    b.Property<string>("Name");

                    b.Property<decimal>("Spirits");

                    b.Property<int>("Status");

                    b.HasKey("Id");

                    b.HasIndex("BrandId");

                    b.ToTable("Products");
                });

            modelBuilder.Entity("Hennessy.Core.Proto.Models.ProductAromatic", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AromaticId");

                    b.Property<DateTime>("LastUpdate");

                    b.Property<string>("Name");

                    b.Property<int>("ProductId");

                    b.Property<int>("Status");

                    b.HasKey("Id");

                    b.HasAlternateKey("AromaticId", "ProductId");

                    b.HasIndex("AromaticId");

                    b.HasIndex("ProductId");

                    b.ToTable("ProductAromatics");
                });

            modelBuilder.Entity("Hennessy.Core.Proto.Models.ProductVolume", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("LastUpdate");

                    b.Property<string>("Name");

                    b.Property<int>("ProductId");

                    b.Property<int>("Status");

                    b.Property<int>("VolumeId");

                    b.HasKey("Id");

                    b.HasAlternateKey("ProductId", "VolumeId");

                    b.HasIndex("ProductId");

                    b.HasIndex("VolumeId");

                    b.ToTable("ProductVolumes");
                });

            modelBuilder.Entity("Hennessy.Core.Proto.Models.ReceptItem", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("Count");

                    b.Property<DateTime>("LastUpdate");

                    b.Property<string>("Name");

                    b.Property<int>("RecipeId");

                    b.Property<int>("Status");

                    b.HasKey("Id");

                    b.HasIndex("RecipeId");

                    b.ToTable("RecipeItems");
                });

            modelBuilder.Entity("Hennessy.Core.Proto.Models.Recipe", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Direction");

                    b.Property<string>("Discription");

                    b.Property<DateTime>("LastUpdate");

                    b.Property<string>("Name");

                    b.Property<int>("Status");

                    b.HasKey("Id");

                    b.ToTable("Recipes");
                });

            modelBuilder.Entity("Hennessy.Core.Proto.Models.Volume", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("LastUpdate");

                    b.Property<string>("Name");

                    b.Property<int>("Status");

                    b.Property<decimal>("Value");

                    b.HasKey("Id");

                    b.ToTable("Volumes");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole", b =>
                {
                    b.Property<string>("Id");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedName")
                        .HasAnnotation("MaxLength", 256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("Hennessy.Core.Proto.Models.Image", b =>
                {
                    b.HasOne("Hennessy.Core.Proto.Models.Aromatic", "Aromatic")
                        .WithOne("Icon")
                        .HasForeignKey("Hennessy.Core.Proto.Models.Image", "AromaticId");

                    b.HasOne("Hennessy.Core.Proto.Models.Brand", "Brand")
                        .WithMany("Images")
                        .HasForeignKey("BrandId");

                    b.HasOne("Hennessy.Core.Proto.Models.Product", "Product")
                        .WithMany()
                        .HasForeignKey("ProductId");

                    b.HasOne("Hennessy.Core.Proto.Models.Recipe", "Recipe")
                        .WithOne("Image")
                        .HasForeignKey("Hennessy.Core.Proto.Models.Image", "RecipeId");
                });

            modelBuilder.Entity("Hennessy.Core.Proto.Models.Product", b =>
                {
                    b.HasOne("Hennessy.Core.Proto.Models.Brand", "Brand")
                        .WithMany("Products")
                        .HasForeignKey("BrandId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Hennessy.Core.Proto.Models.ProductAromatic", b =>
                {
                    b.HasOne("Hennessy.Core.Proto.Models.Aromatic", "Aromatic")
                        .WithMany("Products")
                        .HasForeignKey("AromaticId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Hennessy.Core.Proto.Models.Product", "Product")
                        .WithMany("Aromatics")
                        .HasForeignKey("ProductId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Hennessy.Core.Proto.Models.ProductVolume", b =>
                {
                    b.HasOne("Hennessy.Core.Proto.Models.Product", "Product")
                        .WithMany("Volumes")
                        .HasForeignKey("ProductId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Hennessy.Core.Proto.Models.Volume", "Volume")
                        .WithMany("Products")
                        .HasForeignKey("VolumeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Hennessy.Core.Proto.Models.ReceptItem", b =>
                {
                    b.HasOne("Hennessy.Core.Proto.Models.Recipe", "Recipe")
                        .WithMany("Items")
                        .HasForeignKey("RecipeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Claims")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("Hennessy.Core.Proto.Models.ApplicationUser")
                        .WithMany("Claims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("Hennessy.Core.Proto.Models.ApplicationUser")
                        .WithMany("Logins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Users")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Hennessy.Core.Proto.Models.ApplicationUser")
                        .WithMany("Roles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
