﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Hennessy.Core.Proto.Models.Enum;

namespace Hennessy.Web.Proto.Data.Migrations
{
    public partial class AddTypeToLogs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Type",
                table: "LogMessages",
                nullable: false,
                defaultValue: LogType.Error);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Type",
                table: "LogMessages");
        }
    }
}
