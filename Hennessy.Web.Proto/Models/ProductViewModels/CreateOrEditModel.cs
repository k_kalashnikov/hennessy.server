﻿using Hennessy.Core.Proto.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hennessy.Web.Proto.Models.ProductViewModels
{
    public class CreateOrEditModel : BaseCreateOrEditModel<Product, int>
    {
        public CreateOrEditModel()
        {
            CurGellery = new List<int>();
            Volumes = new List<int>();
            Aromatics = new List<int>();
            NewGallery = new List<IFormFile>();
            Properties = new List<Property>();
            Tags = new List<string>();
            Recipes = new List<int>();

        }
        public CreateOrEditModel(ApplicationDbContext _context, Product _model) : base(_context, _model)
        {
            PosibleAromatic = _context.Aromatics.Include(m => m.Icon)?.ToList() ?? new List<Aromatic>();
            PosibleVolume = _context.Volumes?.ToList() ?? new List<Volume>();
            PosibleBrand = _context.Brands?.ToList() ?? new List<Brand>();
            Properties = model.Properties?.ToList() ?? new List<Property>();
            PosibleTags = _context.Tags.ToList() ?? new List<Tag>();
            PosibleRecipes = _context.Recipes.Include(m => m.Images).ToList() ?? new List<Recipe>();

            CurGellery = new List<int>();
            Volumes = new List<int>();
            Aromatics = new List<int>();
            NewGallery = new List<IFormFile>();
            Tags = new List<string>();
            Recipes = new List<int>();


            var ImgList = model.Images?.Where(m => m.Type == Core.Proto.Models.Enum.ImageType.ProductGallery).ToList() ?? new List<Image>();
            foreach (var item in ImgList)
            {
                CurGellery.Add(item.Id);
            }

            var VolList = model.Volumes?.ToList() ?? new List<ProductVolume>();
            foreach (var item in VolList)
            {
                Volumes.Add(item.VolumeId);
            }

            var AroList = model.Aromatics?.ToList() ?? new List<ProductAromatic>();
            foreach (var item in AroList)
            {
                Aromatics.Add(item.AromaticId);
            }

            var TagList = model.Tags?.ToList() ?? new List<ProductTag>();
            foreach (var item in TagList)
            {
                Tags.Add(item.Tag.Name);
            }

            var RecipeList = model.Recipes?.ToList() ?? new List<ProductRecipe>();
            foreach (var item in RecipeList)
            {
                Recipes.Add(item.RecipeId);
            }
        }

        public List<Brand> PosibleBrand { get; set; } 
        public List<Volume> PosibleVolume { get; set; }
        public List<Aromatic> PosibleAromatic { get; set; }
        public List<Recipe> PosibleRecipes { get; set; }
        public List<Tag> PosibleTags { get; set; }

        public List<Property> Properties { get; set; }
        public List<int> Volumes { get; set; }
        public List<int> Aromatics { get; set; }
        public List<int> CurGellery { get; set; }
        public List<string> Tags { get; set; }
        public List<int> Recipes { get; set; }

        public IFormFile Logo { get; set; }
        public List<IFormFile> NewGallery { get; set; }
    }
}
