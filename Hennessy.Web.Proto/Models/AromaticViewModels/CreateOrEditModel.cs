﻿using Hennessy.Core.Proto.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hennessy.Web.Proto.Models.AromaticViewModels
{
    public class CreateOrEditModel : BaseCreateOrEditModel<Aromatic, int>
    {
        public CreateOrEditModel() : base() { }
        public CreateOrEditModel(ApplicationDbContext _context, Aromatic _model) : base(_context, _model)
        {

        }

        public IFormFile Icon { get; set; }
    }
}
