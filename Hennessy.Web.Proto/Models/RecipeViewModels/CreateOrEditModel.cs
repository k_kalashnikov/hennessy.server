﻿using Hennessy.Core.Proto.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hennessy.Web.Proto.Models.RecipeViewModels
{
    public class CreateOrEditModel : BaseCreateOrEditModel<Recipe, int>
    {
        public CreateOrEditModel(): base()
        {
            CurGellery = new List<int>();
            NewGallery = new List<IFormFile>();
        }

        public CreateOrEditModel(ApplicationDbContext _context, Recipe _model) : base(_context, _model)
        {
            CurGellery = new List<int>();
            NewGallery = new List<IFormFile>();
            var ImgList = model.Images?.Where(m => m.Type == Core.Proto.Models.Enum.ImageType.RecipeGallery).ToList() ?? new List<Image>();
            foreach (var item in ImgList)
            {
                CurGellery.Add(item.Id);
            }
        }

        public IFormFile Logo { get; set; }
        public List<IFormFile> NewGallery { get; set; }
        public List<int> CurGellery { get; set; }
    }
}
