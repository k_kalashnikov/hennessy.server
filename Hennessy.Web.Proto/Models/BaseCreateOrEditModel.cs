﻿using Hennessy.Core.Proto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hennessy.Web.Proto.Models
{
    public class BaseCreateOrEditModel<TModel, TPK> where TModel : BaseModel<TPK>
    {
        public BaseCreateOrEditModel() { }

        public BaseCreateOrEditModel(ApplicationDbContext _context, TModel _model)
        {
            model = _model;
        }
        public TModel model { get; set; }
    }
}
