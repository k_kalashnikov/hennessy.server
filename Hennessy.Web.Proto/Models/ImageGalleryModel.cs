﻿using Hennessy.Core.Proto.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hennessy.Web.Proto.Models
{
    public class ImageGalleryModel : BaseCreateOrEditModel<Image, int>
    {
        public ImageGalleryModel() { }

        public ImageGalleryModel(ApplicationDbContext _context, Image _model) : base(_context, _model)
        {

        }

        public IFormFile File { get; set; }
    }
}
