﻿using Hennessy.Core.Proto.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hennessy.Web.Proto.Models.BrandViewModels
{
    public class CreateOrEditModel : BaseCreateOrEditModel<Brand, int>
    {
        public CreateOrEditModel(): base()
        {
            CurGellery = new List<int>();
            NewGallery = new List<IFormFile>();
        }

        public CreateOrEditModel(ApplicationDbContext _context, Brand _model) : base(_context, _model)
        {
            CurGellery = new List<int>();
            NewGallery = new List<IFormFile>();
            var ImgList = model.Images?.Where(m => m.Type == Core.Proto.Models.Enum.ImageType.BrandGallery).ToList() ?? new List<Image>();
            foreach (var item in ImgList)
            {
                CurGellery.Add(item.Id);
            }
        }

        public IFormFile Logo { get; set; }
        public List<IFormFile> NewGallery { get; set; }
        public List<int> CurGellery { get; set; }
    }
}
