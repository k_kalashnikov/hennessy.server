﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hennessy.Core.Proto.AppConfig;
using Hennessy.Core.Proto.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Hennessy.Web.Proto.Controllers
{
    public class ExportController : BaseController
    {
        public ExportController(ApplicationDbContext _context, IOptions<DirectoriesOptions> _directories) : base(_context, _directories)
        {
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }
    }
}
