﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Hennessy.Core.Proto.Models;
using Hennessy.Core.Proto.AppConfig;
using Hennessy.Core.Proto.Tools;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.IO;
using Hennessy.Core.Proto.Models.Enum;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Hennessy.Web.Proto.Controllers
{
    public class BaseController : Controller
    {
        protected ApplicationDbContext context { get; set; }
        protected DirectoriesOptions directories { get; set; }
        protected ModelLogger logger { get; set; }

        public BaseController(ApplicationDbContext _context, IOptions<DirectoriesOptions> _directories)
        {
            context = _context;
            directories = _directories.Value;
            logger = new ModelLogger(context);
        }

        [HttpGet]
        public virtual IActionResult GetImage(int id = -1)
        {
            try
            {
                var ImageFromDb = context.Images.FirstOrDefault(m => m.Id == id);
                var uploadDirectory = directories.GetPathByType(ImageFromDb.Type);
                var stream = System.IO.File.OpenRead(Path.Combine(uploadDirectory, ImageFromDb.Name));
                return new FileStreamResult(stream, GetMimeType(ImageFromDb.Name));
            }
            catch (Exception ex)
            {
                logger.Add(ex.Message, LogType.Error);
                return NoContent();
            }
        }

        public virtual string GetMimeType(string filename)
        {
            string contentType;
            new FileExtensionContentTypeProvider().TryGetContentType(filename, out contentType);
            return contentType ?? "application/octet-stream";
        }

        public virtual List<string> GetModelStateErrors(ModelStateDictionary modelState)
        {
            List<string> result = new List<string>();
            foreach (var itemVal in modelState.Values)
            {
                foreach (var itemError in itemVal.Errors)
                {
                    result.Add(itemError.ErrorMessage);
                }
            }
            return result;
        }
    }
}
