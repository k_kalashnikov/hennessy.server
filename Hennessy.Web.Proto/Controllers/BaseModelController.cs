﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Hennessy.Core.Proto.Models;
using Hennessy.Core.Proto.Repository;
using Microsoft.AspNetCore.Authorization;
using Hennessy.Core.Proto.AppConfig;
using System.IO;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Security.Cryptography;
using Microsoft.CodeAnalysis.Options;
using Microsoft.Extensions.Options;
using Hennessy.Web.Proto.Models;
using Hennessy.Core.Proto.Tools;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Hennessy.Web.Proto.Controllers
{
    [Authorize]
    public class BaseModelController<TModel, TPK> : BaseController where TModel : BaseModel<TPK>
    {
        protected UnitOfWorks unit { get; set; }
        protected BaseRepository<TModel, TPK> repository { get; set; }

        public BaseModelController(ApplicationDbContext _context, IOptions<DirectoriesOptions> _directories) : base(_context, _directories)
        {
            unit = new UnitOfWorks(context);
        }

        [HttpGet]
        public virtual IActionResult Index()
        {
            var result = repository.GetAll();
            return View(result);
        }

        [HttpGet]
        public virtual IActionResult Detail(TPK id)
        {
            var result = repository.GetById(id);
            return View(result);
        }

        [HttpPost]
        public virtual object Delete(TPK id)
        {
            var result = repository.DeleteById(id);
            unit.Commit();
            return new
            {
                message = result.Message,
                result = result.Status
            };
        }

        [HttpGet]
        public virtual IActionResult CreateOrEdit(TPK id, int operationId = -1)
        {
            var model = repository.GetById(id) ?? Activator.CreateInstance(typeof(TModel));
            var result = new BaseCreateOrEditModel<TModel,TPK>(context, model as TModel);
            ViewBag.Messages = logger.GetByOpration(operationId);
            return View(result);
        }

        public virtual string GetHashName(Stream _stream)
        {
            MD5 md5 = MD5.Create();
            byte[] fileData = new byte[_stream.Length];
            _stream.Read(fileData, 0, (int)_stream.Length);
            byte[] checkSum = md5.ComputeHash(fileData);
            string result = BitConverter.ToString(checkSum).Replace("-", String.Empty);
            return result;
        }

    }
}
