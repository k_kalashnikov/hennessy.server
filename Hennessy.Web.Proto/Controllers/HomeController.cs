﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Hennessy.Core.Proto.AppConfig;
using Hennessy.Core.Proto.Models;
using Microsoft.Extensions.Options;

namespace Hennessy.Web.Proto.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        public HomeController(ApplicationDbContext _context, IOptions<DirectoriesOptions> _directories) : base(_context, _directories)
        {
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";
            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }

        public IActionResult Logs()
        {
            var result = logger.GetAll();
            return View(result);
        }

        public IActionResult Email()
        {
            return View();
        }
    }
}
