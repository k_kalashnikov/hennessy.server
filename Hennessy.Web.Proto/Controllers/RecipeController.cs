﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Hennessy.Core.Proto.Models;
using Hennessy.Core.Proto.AppConfig;
using Microsoft.Extensions.Options;
using Hennessy.Web.Proto.Models.RecipeViewModels;
using Hennessy.Core.Proto.Models.Enum;
using System.IO;
using Hennessy.Web.Proto.Models;

namespace Hennessy.Web.Proto.Controllers
{
    public class RecipeController : BaseModelController<Recipe, int>
    {
        public RecipeController(ApplicationDbContext _context, IOptions<DirectoriesOptions> _directories) : base(_context, _directories)
        {
            repository = unit.Recipes;
        }

        [HttpGet]
        public override IActionResult CreateOrEdit(int id, int operationId = -1)
        {

            var model = repository.GetById(id) ?? new Recipe();
            var result = new CreateOrEditModel(context, model);
            ViewBag.Messages = logger.GetByOpration(operationId);
            return View(result);
        }

        [HttpPost]
        public IActionResult CreateOrEdit(CreateOrEditModel _formResult)
        {
            if (ModelState.IsValid)
            {
                var Messages = new List<LogMessage>();
                Recipe model = _formResult.model;
                unit.Images.GetAll().Where(m => (m.RecipeId == model.Id) && (m.Type == ImageType.RecipeGallery)).ToList().ForEach(m => { m.RecipeId = null; m.Recipe = null; unit.Images.Update(m); });
                unit.Commit();

                model.Images = new List<Image>();
                string upload = directories.GetPathByType(ImageType.RecipeGallery);
                if (_formResult.Logo != null)
                {
                    unit.Images.GetAll().Where(m => (m.RecipeId == model.Id) && (m.Type == ImageType.RecipeLogo)).ToList().ForEach(m => { m.RecipeId = null; m.Recipe = null; unit.Images.Update(m); });
                    using (var fileStream = new FileStream(Path.Combine(upload, _formResult.Logo.FileName), FileMode.OpenOrCreate))
                    {
                        _formResult.Logo.CopyTo(fileStream);
                    }
                    Image tempImg = new Image()
                    {
                        Type = ImageType.RecipeLogo,
                        Name = _formResult.Logo.FileName,
                        Recipe = model
                    };
                    Messages.Add(unit.Images.Add(tempImg).Message);
                    model.Images.Add(tempImg);
                }
                else
                {
                    model.Images.Add(unit.Images.GetAll().FirstOrDefault(m => (m.RecipeId == model.Id) && (m.Type == ImageType.RecipeLogo)));
                }

                foreach (var item in _formResult.CurGellery)
                {
                    Image tempImg = unit.Images.GetById(item);
                    if (tempImg == null)
                    {
                        continue;
                    }
                    tempImg.Recipe = model;
                    tempImg.Type = ImageType.RecipeGallery;
                    Messages.Add(unit.Images.Update(tempImg).Message);
                    model.Images.Add(tempImg);
                }

                foreach (var item in _formResult.NewGallery)
                {
                    Image tempImg = new Image()
                    {
                        Name = item.FileName
                    };
                    tempImg.Type = ImageType.RecipeGallery;
                    tempImg.Recipe = model;
                    using (var fileStream = new FileStream(Path.Combine(upload, item.FileName), FileMode.OpenOrCreate))
                    {
                        item.CopyTo(fileStream);
                    }
                    Messages.Add(unit.Images.Add(tempImg).Message);
                    model.Images.Add(tempImg);
                }
                var result = (model.Id > 0) ? repository.Update(model) : repository.Add(model);
                Messages.Add(result.Message);
                unit.Commit();
                return RedirectToAction("CreateOrEdit", new { id = result.Object.Id, operationId = logger.Add(Messages) });
            }
            return RedirectToAction("CreateOrEdit", new { id = -1, operationId = logger.Add(GetModelStateErrors(ModelState), LogType.Error) });
        }
    }
}
