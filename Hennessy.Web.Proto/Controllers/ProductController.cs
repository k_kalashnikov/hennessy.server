﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Hennessy.Core.Proto.Models;
using Hennessy.Core.Proto.AppConfig;
using Microsoft.Extensions.Options;
using Hennessy.Web.Proto.Models.ProductViewModels;
using Hennessy.Core.Proto.Models.Enum;
using System.IO;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Hennessy.Web.Proto.Controllers
{
    public class ProductController : BaseModelController<Product, int>
    {
        public ProductController(ApplicationDbContext _context, IOptions<DirectoriesOptions> _directories) : base(_context, _directories)
        {
            repository = unit.Products;
        }

        [HttpGet]
        public override IActionResult CreateOrEdit(int id, int operationId = -1)
        {
            var model = repository.GetById(id) ?? new Product();
            var result = new CreateOrEditModel(context, model);
            ViewBag.Messages = logger.GetByOpration(operationId);
            return View(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CreateOrEdit(CreateOrEditModel _formResult)
        {
            if (ModelState.IsValid)
            {
                var Messages = new List<LogMessage>();
                Product model = _formResult.model;

                unit.ProductVolumes.GetAll().Where(m => (m.ProductId == model.Id))?.ToList()?.ForEach(m => unit.ProductVolumes.Remove(m));
                unit.ProductAromatics.GetAll().Where(m => (m.ProductId == model.Id))?.ToList()?.ForEach(m => unit.ProductAromatics.Remove(m));
                unit.Properties.GetAll().Where(m => m.ProductId == model.Id)?.ToList()?.ForEach(m => unit.Properties.Remove(m));
                unit.Images.GetAll().Where(m => (m.ProductId == model.Id) && (m.Type == ImageType.ProductGallery)).ToList().ForEach(m => { m.ProductId = null; m.Product = null; unit.Images.Update(m); });
                unit.ProductTags.Find(m => m.ProductId == model.Id)?.ToList().ForEach(m => unit.ProductTags.Remove(m));
                unit.ProductRecipes.Find(m => m.ProductId == model.Id)?.ToList().ForEach(m => unit.ProductRecipes.Remove(m));
                unit.Commit();

                model.Aromatics = new List<ProductAromatic>();
                model.Volumes = new List<ProductVolume>();
                model.Properties = new List<Property>();
                model.Images = new List<Image>();
                model.Tags = new List<ProductTag>();
                model.Recipes = new List<ProductRecipe>();

                foreach (var item in _formResult.Aromatics)
                {
                    ProductAromatic tempArm = new ProductAromatic()
                    {
                        AromaticId = item,
                        Product = model
                    };
                    unit.ProductAromatics.Add(tempArm);
                    model.Aromatics.Add(tempArm);
                }

                foreach (var item in _formResult.Volumes)
                {
                    ProductVolume tempVol = new ProductVolume()
                    {
                        VolumeId = item,
                        Product = model
                    };
                    unit.ProductVolumes.Add(tempVol);
                    model.Volumes.Add(tempVol);
                }

                foreach (var item in _formResult.Properties)
                {
                    Property tempProp = new Property()
                    {
                        Product = model,
                        Name = item.Name,
                        Value = item.Value,
                        Type = PropertyType.String
                    };
                    unit.Properties.Add(tempProp);
                    model.Properties.Add(tempProp);
                }

                string upload = directories.GetPathByType(ImageType.ProductGallery);
                if (_formResult.Logo != null)
                {
                    unit.Images.GetAll().Where(m => (m.ProductId == model.Id) && (m.Type == ImageType.ProductLogo)).ToList().ForEach(m => { m.ProductId = null; m.Product = null; unit.Images.Update(m); });
                    using (var fileStream = new FileStream(Path.Combine(upload, _formResult.Logo.FileName), FileMode.OpenOrCreate))
                    {
                        _formResult.Logo.CopyTo(fileStream);
                    }
                    Image tempImg = new Image()
                    {
                        Type = ImageType.ProductLogo,
                        Name = _formResult.Logo.FileName,
                        Product = model
                    };
                    Messages.Add(unit.Images.Add(tempImg).Message);
                    model.Images.Add(tempImg);
                }
                else
                {
                    model.Images.Add(unit.Images.GetAll().FirstOrDefault(m => (m.ProductId == model.Id) && (m.Type == ImageType.ProductLogo)));
                }

                foreach (var item in _formResult.CurGellery)
                {
                    Image tempImg = unit.Images.GetById(item);
                    if (tempImg == null)
                    {
                        continue;
                    }
                    tempImg.Product = model;
                    tempImg.Type = ImageType.ProductGallery;
                    Messages.Add(unit.Images.Update(tempImg).Message);
                    model.Images.Add(tempImg);
                }

                foreach (var item in _formResult.NewGallery)
                {
                    Image tempImg = new Image()
                    {
                        Name = item.FileName
                    };
                    tempImg.Type = ImageType.ProductGallery;
                    tempImg.Product = model;
                    using (var fileStream = new FileStream(Path.Combine(upload, item.FileName), FileMode.OpenOrCreate))
                    {
                        item.CopyTo(fileStream);
                    }
                    Messages.Add(unit.Images.Add(tempImg).Message);
                    model.Images.Add(tempImg);
                }

                foreach (var item in _formResult.Tags)
                {
                    var tag = unit.Tags.Find(m => m.Name.Equals(item.ToLower().Trim())).FirstOrDefault();
                    if (tag == null)
                    {
                        tag = new Tag()
                        {
                            Name = item.ToLower().Trim()
                        };
                        Messages.Add(unit.Tags.Add(tag).Message);
                    }
                    var prodTag = new ProductTag()
                    {
                        Tag = tag,
                        Product = model
                    };
                    unit.ProductTags.Add(prodTag);
                    model.Tags.Add(prodTag);
                }

                foreach (var item in _formResult.Recipes)
                {
                    ProductRecipe tempPR = new ProductRecipe()
                    {
                        Product = model,
                        RecipeId = item
                    };
                    unit.ProductRecipes.Add(tempPR);
                    model.Recipes.Add(tempPR);
                }

                var result = (model.Id > 0) ? repository.Update(model) : repository.Add(model);
                Messages.Add(result.Message);
                unit.Commit();
                return RedirectToAction("CreateOrEdit", new { id = result.Object.Id, operationId = logger.Add(Messages) });
            }
            return RedirectToAction("CreateOrEdit", new { id = -1, operationId = logger.Add(GetModelStateErrors(ModelState), LogType.Error) });
        }

        [HttpPost]
        public IActionResult GetPropertyForm(int index)
        {
            ViewBag.Index = index;
            var result = new CreateOrEditModel();
            return View(result);
        }
    }
}
