﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Hennessy.Core.Proto.Models;
using Hennessy.Core.Proto.AppConfig;
using Microsoft.Extensions.Options;
using Hennessy.Web.Proto.Models.AromaticViewModels;
using Hennessy.Core.Proto.Models.Enum;
using System.IO;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Hennessy.Web.Proto.Controllers
{
    public class AromaticController : BaseModelController<Aromatic, int>
    {
        public AromaticController(ApplicationDbContext _context, IOptions<DirectoriesOptions> _directories) : base(_context, _directories)
        {
            repository = unit.Aromatics;
        }

        //[HttpGet]
        //public override IActionResult CreateOrEdit(int id, int? operationId)
        //{
        //    var model = repository.GetById(id) ?? new Aromatic();
        //    var result = new CreateOrEditModel(context, model);
        //    ViewBag.Messages = _messages ?? new List<string>();
        //    return View(result);
        //}

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CreateOrEdit(CreateOrEditModel _formResult)
        {
            if (ModelState.IsValid)
            {
                var Messages = new List<LogMessage>();
                var model = _formResult.model;
                if (_formResult.Icon != null)
                {
                    unit.Images.GetAll().Where(m => m.AromaticId == model.Id).ToList().ForEach(m => { m.AromaticId = null; m.Aromatic = null; Messages.Add(unit.Images.Update(m).Message); });
                    unit.Commit();
                    string upload = directories.GetPathByType(ImageType.AromaticIcon);
                    using (var fileStream = new FileStream(Path.Combine(upload, _formResult.Icon.FileName), FileMode.OpenOrCreate))
                    {
                        _formResult.Icon.CopyTo(fileStream);
                    }
                    Image tempImg = new Image()
                    {
                        Aromatic = model,
                        Name = _formResult.Icon.FileName,
                        Type = ImageType.AromaticIcon
                    };
                    Messages.Add(unit.Images.Add(tempImg).Message);
                    model.Icon = tempImg;
                }
                var result = (model.Id > 0) ? repository.Update(model) : repository.Add(model);
                Messages.Add(result.Message);
                unit.Commit();
                return RedirectToAction("CreateOrEdit", new { id = result.Object.Id, operationId = logger.Add(Messages) });
            }
            return RedirectToAction("CreateOrEdit", new { id = -1, operationId = logger.Add(GetModelStateErrors(ModelState), LogType.Error) });
        }
    }
}
