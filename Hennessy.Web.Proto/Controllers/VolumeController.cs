﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Hennessy.Core.Proto.Models;
using Hennessy.Core.Proto.AppConfig;
using Microsoft.Extensions.Options;
using Hennessy.Web.Proto.Models;
using Hennessy.Core.Proto.Models.Enum;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Hennessy.Web.Proto.Controllers
{
    public class VolumeController : BaseModelController<Volume, int>
    {
        public VolumeController(ApplicationDbContext _context, IOptions<DirectoriesOptions> _directories) : base(_context, _directories)
        {
            repository = unit.Volumes;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CreateOrEdit(BaseCreateOrEditModel<Volume, int> _formResult)
        {
            if (ModelState.IsValid)
            {
                var Messages = new List<LogMessage>();
                var result = (_formResult.model.Id > 0) ? repository.Update(_formResult.model) : repository.Add(_formResult.model);
                Messages.Add(result.Message);
                unit.Commit();
                return RedirectToAction("CreateOrEdit", new { id = result.Object.Id, operationId = logger.Add(Messages) });
            }
            return RedirectToAction("CreateOrEdit", new { id = -1, operationId = logger.Add(GetModelStateErrors(ModelState), LogType.Error)});
        }
    }
}
