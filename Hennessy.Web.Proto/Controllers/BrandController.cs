﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Hennessy.Core.Proto.Models;
using Hennessy.Web.Proto.Models.BrandViewModels;
using System.IO;
using Hennessy.Core.Proto.Models.Enum;
using Hennessy.Core.Proto.AppConfig;
using Microsoft.Extensions.Options;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Hennessy.Web.Proto.Controllers
{
    public class BrandController : BaseModelController<Brand, int>
    {
        public BrandController(ApplicationDbContext _context, IOptions<DirectoriesOptions> _directories) : base(_context, _directories)
        {
            repository = unit.Brands;
        }

        [HttpGet]
        public override IActionResult CreateOrEdit(int id, int operationId = -1)
        {
            var model = repository.GetById(id) ?? new Brand();
            var result = new CreateOrEditModel(context, model);
            ViewBag.Messages = logger.GetByOpration(operationId);
            return View(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CreateOrEdit(CreateOrEditModel _formResult)
        {
            if (ModelState.IsValid)
            {
                var Messages = new List<LogMessage>();
                Brand model = _formResult.model;
                unit.Images.GetAll().Where(m => (m.BrandId == model.Id) && (m.Type == ImageType.BrandGallery)).ToList().ForEach(m => { m.BrandId = null; m.Brand = null; unit.Images.Update(m);});
                unit.Commit();

                model.Images = new List<Image>();
                string upload = directories.GetPathByType(ImageType.BrandGallery);
                if (_formResult.Logo != null)
                {
                    unit.Images.GetAll().Where(m => (m.BrandId == model.Id) && (m.Type == ImageType.BrandLogo)).ToList().ForEach(m => { m.BrandId = null; m.Brand = null; unit.Images.Update(m); });
                    using (var fileStream = new FileStream(Path.Combine(upload, _formResult.Logo.FileName), FileMode.OpenOrCreate))
                    {
                        _formResult.Logo.CopyTo(fileStream);
                    }
                    Image tempImg = new Image()
                    {
                        Type = ImageType.BrandLogo,
                        Name = _formResult.Logo.FileName,
                        Brand = model
                    };
                    Messages.Add(unit.Images.Add(tempImg).Message);
                    model.Images.Add(tempImg);
                }
                else
                {
                    model.Images.Add(unit.Images.GetAll().FirstOrDefault(m => (m.BrandId == model.Id) && (m.Type == ImageType.BrandLogo)));
                }

                foreach (var item in _formResult.CurGellery)
                {
                    Image tempImg = unit.Images.GetById(item);
                    if (tempImg == null)
                    {
                        continue;
                    }
                    tempImg.Brand = model;
                    tempImg.Type = ImageType.BrandGallery;
                    Messages.Add(unit.Images.Update(tempImg).Message);
                    model.Images.Add(tempImg);
                }

                foreach (var item in _formResult.NewGallery)
                {
                    Image tempImg = new Image()
                    {
                        Name = item.FileName
                    };
                    tempImg.Type = ImageType.BrandGallery;
                    tempImg.Brand = model;
                    using (var fileStream = new FileStream(Path.Combine(upload, item.FileName), FileMode.OpenOrCreate))
                    {
                        item.CopyTo(fileStream);
                    }
                    Messages.Add(unit.Images.Add(tempImg).Message);
                    model.Images.Add(tempImg);
                }
                var result = (model.Id > 0) ? repository.Update(model) : repository.Add(model);
                Messages.Add(result.Message);
                unit.Commit();
                return RedirectToAction("CreateOrEdit", new { id = result.Object.Id, operationId = logger.Add(Messages) });
            }
            return RedirectToAction("CreateOrEdit", new { id = -1, operationId = logger.Add(GetModelStateErrors(ModelState), LogType.Error) });
        }

        [HttpPost]
        public IActionResult AddImageForm(int index)
        {
            ViewBag.Index = index;
            return View();
        }
    }
}
