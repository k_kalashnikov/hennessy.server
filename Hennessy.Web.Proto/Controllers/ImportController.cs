﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hennessy.Core.Proto.AppConfig;
using Hennessy.Core.Proto.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Hennessy.Core.Proto.Models.Enum;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Hennessy.Web.Proto.Controllers
{
    public class ImportController : BaseController
    {
        public ImportController(ApplicationDbContext _context, IOptions<DirectoriesOptions> _directories) : base(_context, _directories)
        {
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Result()
        {
            List<Log> result = new List<Log>();
            for (int j = 0; j < 10; j++)
            {
                Log temp = new Log()
                {
                    Id = j,
#pragma warning disable CS1701 // Предполагается, что ссылка на сборку совпадает с удостоверением
                    Messages = new List<LogMessage>()
#pragma warning restore CS1701 // Предполагается, что ссылка на сборку совпадает с удостоверением
                };

                for (int i = 0; i < 5; i++)
                {
                    LogMessage tempMessage = new LogMessage()
                    {
                        Message = "Error message",
                        Type = LogType.Error,
                        Id = i
                    };
                    temp.Messages.Add(tempMessage);
                }

                for (int i = 0; i < 3; i++)
                {
                    LogMessage tempMessage = new LogMessage()
                    {
                        Message = "Warning message",
                        Type = LogType.Warning,
                        Id = i
                    };
                    temp.Messages.Add(tempMessage);
                }

                for (int i = 0; i < 8; i++)
                {
                    LogMessage tempMessage = new LogMessage()
                    {
                        Message = "Success message",
                        Type = LogType.Success,
                        Id = i
                    };
                    temp.Messages.Add(tempMessage);
                }
#pragma warning disable CS1701 // Предполагается, что ссылка на сборку совпадает с удостоверением
                result.Add(temp);
#pragma warning restore CS1701 // Предполагается, что ссылка на сборку совпадает с удостоверением
            }


            return View(result);
        }
    }
}
