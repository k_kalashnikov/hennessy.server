﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hennessy.Core.Proto.AppConfig
{
    public class SmtpConfig
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public bool UseTLS { get; set; }
        public string User { get; set; }
        public string Password { get; set; }

        public string FromName { get; set; }
        public string FromEmail { get; set; }
    }
}
