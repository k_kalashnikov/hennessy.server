﻿using Hennessy.Core.Proto.Models.Enum;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Hennessy.Core.Proto.AppConfig
{
    public class DirectoriesOptions
    {
        public string ImagesDirectoriesBase { get; set; }
        public string BrandImagesDirectories { get; set; }
        public string ProductImagesDirectories { get; set; }
        public string RecipeImagesDirectories { get; set; }
        public string AromaticImagesDirectories { get; set; }


        public string GetPathByType(ImageType _type)
        {
            switch (_type)
            {
                case ImageType.BrandGallery:
                case ImageType.BrandLogo:
                    return Path.Combine(ImagesDirectoriesBase, BrandImagesDirectories);
                case ImageType.ProductGallery:
                case ImageType.ProductLogo:
                    return Path.Combine(ImagesDirectoriesBase, BrandImagesDirectories);
                case ImageType.RecipeLogo:
                case ImageType.RecipeGallery:
                    return Path.Combine(ImagesDirectoriesBase, RecipeImagesDirectories);
                case ImageType.AromaticIcon:
                    return Path.Combine(ImagesDirectoriesBase, AromaticImagesDirectories);
                default:
                    return ImagesDirectoriesBase;
            }
        }
    }
}
