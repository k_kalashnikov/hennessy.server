﻿using Hennessy.Core.Proto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Hennessy.Core.Proto.Repository
{
    public class AromaticRepository : BaseRepository<Aromatic, int>
    {
        public AromaticRepository(ApplicationDbContext _context) : base(_context)
        {
        }

        public override IQueryable<Aromatic> GetAllBase()
        {
            return entitySet.Include(m => m.Icon).Include(m => m.Products).ThenInclude(m => m.Product);
        }
    }
}
