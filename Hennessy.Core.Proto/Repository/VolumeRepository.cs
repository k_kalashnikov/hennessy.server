﻿using Hennessy.Core.Proto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Hennessy.Core.Proto.Repository
{
    public class VolumeRepository : BaseRepository<Volume, int>
    {
        public VolumeRepository(ApplicationDbContext _context) : base(_context)
        {
        }

        public override IQueryable<Volume> GetAllBase()
        {
            return entitySet.Include(m => m.Products).ThenInclude(m => m.Product);
        }
    }
}
