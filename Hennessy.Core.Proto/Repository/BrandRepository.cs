﻿using Hennessy.Core.Proto.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hennessy.Core.Proto.Repository
{
    public class BrandRepository : BaseRepository<Brand, int>
    {
        public BrandRepository(ApplicationDbContext _context) : base(_context)
        {
        }

        public override IQueryable<Brand> GetAllBase()
        {
            return entitySet.Include(m => m.Images).Include(m => m.Products);
        }
    }
}
