﻿using Hennessy.Core.Proto.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hennessy.Core.Proto.Repository
{
    public class ImageRepository : BaseRepository<Image, int>
    {
        public ImageRepository(ApplicationDbContext _context) : base(_context)
        {
        }

        public override IQueryable<Image> GetAllBase()
        {
            return entitySet.Include(m => m.Aromatic).Include(m => m.Brand).Include(m => m.Product).Include(m => m.Recipe);
        }
    }
}
