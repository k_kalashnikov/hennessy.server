﻿using Hennessy.Core.Proto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Hennessy.Core.Proto.Repository
{
    public class UnitOfWorks : IDisposable
    {
        public BrandRepository Brands { get; set; }
        public ProductRepository Products { get; set; }
        public ImageRepository Images { get; set; }
        public RecipeRepository Recipes { get; set; }
        public AromaticRepository Aromatics { get; set; }
        public VolumeRepository Volumes { get; set; }
        public BaseRepository<ProductAromatic, int> ProductAromatics { get; set; }
        public BaseRepository<ProductVolume, int> ProductVolumes { get; set; }
        public BaseRepository<ProductRecipe, int> ProductRecipes { get; set; }
        public BaseRepository<Property, int> Properties { get; set; }
        public BaseRepository<Tag, int> Tags { get; set; }
        public BaseRepository<ProductTag, int> ProductTags { get; set; }
        protected ApplicationDbContext context { get; private set; }

        public UnitOfWorks(ApplicationDbContext _context)
        {
            context = _context;
            var properties = this.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var property in properties)
            {
                var instance = Activator.CreateInstance(property.PropertyType, this.context);
                property.SetValue(this, instance, null);
            }
        }

        public int Commit()
        {
            return context.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
