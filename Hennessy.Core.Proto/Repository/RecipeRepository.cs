﻿using Hennessy.Core.Proto.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hennessy.Core.Proto.Repository
{
    public class RecipeRepository : BaseRepository<Recipe, int>
    {
        public RecipeRepository(ApplicationDbContext _context) : base(_context)
        {
        }

        public override IQueryable<Recipe> GetAllBase()
        {
            return entitySet.Include(m => m.Images).Include(m => m.Items).Include(m => m.Products).ThenInclude(m => m.Product);
        }
    }
}
