﻿using Hennessy.Core.Proto.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hennessy.Core.Proto.Repository
{
    public class ProductRepository : BaseRepository<Product, int>
    {
        public ProductRepository(ApplicationDbContext _context) : base(_context)
        {
        }

        public override IQueryable<Product> GetAllBase()
        {
            return entitySet.Include(m => m.Aromatics).ThenInclude(m => m.Aromatic).
                Include(m => m.Volumes).ThenInclude(m => m.Volume).
                Include(m => m.Recipes).ThenInclude(m => m.Recipe).
                Include(m => m.Images).Include(m => m.Brand).Include(m => m.Properties).
                Include(m => m.Tags).ThenInclude(m => m.Tag);
        }
    }
}
