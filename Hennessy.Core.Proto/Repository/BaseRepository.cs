﻿using Hennessy.Core.Proto.Models;
using Hennessy.Core.Proto.Models.Enum;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hennessy.Core.Proto.Repository
{
    public class BaseRepository<TModel, TPK> where TModel : BaseModel<TPK>
    {
        protected ApplicationDbContext context { get; set; }
        protected DbSet<TModel> entitySet { get; set; }

        public BaseRepository(ApplicationDbContext _context)
        {
            if (_context == null)
            {
                throw new ArgumentNullException(nameof(_context), "Контекст базы данных не передан или равен NULL");
            }

            context = _context;
            entitySet = context.Set<TModel>();
        }

        public virtual Result<TModel> Add(TModel item)
        {
            try
            {
                TModel result = entitySet.Add(item).Entity;
                return new Result<TModel>(result, $"Элемент {item.Name} успешно добавлен");
            }
            catch (Exception e)
            {
                return new Result<TModel>(item, e.Message, LogType.Error, false);
            }
        }

        public virtual Result<TModel> Delete(TModel item)
        {
            try
            {
                item.Status = RowStatus.Delete;
                TModel result = entitySet.Update(item).Entity;
                return new Result<TModel>(result, $"Элемент {item.Name}  успешно удалён");
            }
            catch(Exception e)
            {
                return new Result<TModel>(item, e.Message, LogType.Error, false);
            }
        }

        public virtual Result<TModel> DeleteById(TPK id)
        {
            TModel item = GetById(id);
            if (item == null)
            {
                return new Result<TModel>(null, $"Элемент c id = {id} не найден", LogType.Error, false);
            }
            return Delete(item);
        }

        public virtual TModel GetById(TPK id)
        {
            return GetAll().FirstOrDefault(m => m.Id.Equals(id));
        }

        public virtual IQueryable<TModel> GetAllBase()
        {
            return entitySet;
        }

        public virtual IQueryable<TModel> GetAll()
        {
            return GetAllBase().Where(m => m.Status == RowStatus.Normal);
        }

        public virtual IQueryable<TModel> GetAll(List<RowStatus> withStatuses)
        {
            withStatuses = withStatuses ?? new List<RowStatus>() { RowStatus.Normal };
            return GetAllBase().Where(m => withStatuses.Contains(m.Status));
        }

        public virtual IQueryable<TModel> GetAll(Expression<Func<TModel, TPK>> orderBy)
        {
            return GetAll().OrderBy(orderBy);
        }

        public virtual IQueryable<TModel> Find(Expression<Func<TModel, bool>> predicate)
        {
            return GetAll().Where(predicate);
        }

        public virtual IQueryable<TModel> Find(Expression<Func<TModel, bool>> predicate, List<RowStatus> withStatuses)
        {
            return GetAll(withStatuses).Where(predicate);
        }

        public Result<TModel> Update(TModel item)
        {
            try
            {
                TModel result = entitySet.Update(item).Entity;
                return new Result<TModel>(result, $"Элемент {item.Name} успешно обновлен");
            }
            catch (Exception e)
            {
                return new Result<TModel>(item, e.Message, LogType.Error, false);
            }
        }

        public Result<TModel> Remove(TModel item)
        {
            try
            {
                entitySet.Remove(item);
                return new Result<TModel>(item, $"Элемент типа {item.GetType().Name} удалён из базы");
            }
            catch (Exception e)
            {
                return new Result<TModel>(item, e.Message, LogType.Error, false);
            }
        }
    }
}
