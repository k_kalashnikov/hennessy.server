﻿using Hennessy.Core.Proto.Models;
using Hennessy.Core.Proto.Models.Enum;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hennessy.Core.Proto.Tools
{
    public class ModelLogger
    {
        protected ApplicationDbContext context { get; set; }

        public ModelLogger(ApplicationDbContext _context)
        {
            context = _context;
        }

        public int Add(List<string> messages, LogType type = LogType.Success)
        {
            Log model = new Log()
            {
                Messages = new List<LogMessage>()
            };
            foreach (var item in messages)
            {
                LogMessage newMessage = new LogMessage()
                {
                    Log = model,
                    Message = item,
                    Type = type
                };
                context.Add(newMessage);
                model.Messages.Add(newMessage);
            }
            var result = context.Add(model).Entity;
            context.SaveChanges();
            return result.Id;
        }

        public int Add(List<LogMessage> messages)
        {
            Log model = new Log()
            {
                Messages = new List<LogMessage>()
            };
            foreach (var item in messages)
            {
                item.Log = model;
                context.Add(item);
                model.Messages.Add(item);
            }
            var result = context.Add(model).Entity;
            context.SaveChanges();
            return result.Id;
        }

        public int Add(string message, LogType type = LogType.Success)
        {
            Log model = new Log()
            {
                Messages = new List<LogMessage>()
            };
            LogMessage newMessage = new LogMessage()
            {
                Log = model,
                Message = message,
                Type = type
            };
            context.Add(newMessage);
            model.Messages.Add(newMessage);
            var result = context.Add(model).Entity;
            context.SaveChanges();
            return result.Id;
        }

        public Log GetByOpration(int operationId)
        {
            return context.Logs.Include(m => m.Messages).FirstOrDefault(m => m.Id == operationId) ?? new Log() { Messages = new List<LogMessage>() };
        }

        public List<Log> GetAll()
        {
            return context.Logs.Include(m => m.Messages)?.ToList() ?? new List<Log>();
        }
    }
}
