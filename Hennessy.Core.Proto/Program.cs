﻿using Hennessy.Core.Proto.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hennessy.Core.Proto
{
    public class Program
    {
        public static void Main(string[] args)
        {
        }

        public class MigrationsContextFactory : IDbContextFactory<ApplicationDbContext>
        {
            public ApplicationDbContext Create(DbContextFactoryOptions options)
            {
                var builder = new DbContextOptionsBuilder<ApplicationDbContext>();
                builder.UseSqlServer("Server=localhost;Database=hennessy;Integrated Security=yes;Trusted_Connection=True;MultipleActiveResultSets=true;", b => b.MigrationsAssembly("Hennessy.Web.Proto"));
                return new ApplicationDbContext(builder.Options);
            }
        }
    }
}
