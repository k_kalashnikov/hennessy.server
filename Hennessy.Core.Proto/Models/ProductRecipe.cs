﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hennessy.Core.Proto.Models
{
    public class ProductRecipe : BaseModel<int>
    {
        public int ProductId { get; set; }
        public int RecipeId { get; set; }
        public Product Product { get; set; }
        public Recipe Recipe { get; set; }
    }
}
