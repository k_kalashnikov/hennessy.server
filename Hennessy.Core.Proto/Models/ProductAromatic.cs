﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Hennessy.Core.Proto.Models
{
    public class ProductAromatic : BaseModel<int>
    {
        public int ProductId { get; set; }
        public int AromaticId { get; set; }

        public Product Product { get; set; }
        public Aromatic Aromatic { get; set; }
    }
}
