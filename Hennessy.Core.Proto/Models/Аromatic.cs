﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hennessy.Core.Proto.Models
{
    /// <summary>
    /// Модель сущности описывающие отдельно взятое ароматическое свойство
    /// </summary>
    public class Aromatic : BaseModel<int>
    {
        /// <summary>
        /// Иконка для отображения в фильтре
        /// </summary>
        public Image Icon { get; set; }
        public virtual ICollection<ProductAromatic> Products { get; set; }
    }
}
