﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hennessy.Core.Proto.Models
{
    /// <summary>
    /// Модель описывающие отдельно взятый бренд
    /// </summary>
    public class Brand : BaseModel<int>
    {
        /// <summary>
        /// Детальное описание бренда
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Список продуктов принадлежащих данному бренду
        /// </summary>
        public virtual ICollection<Product> Products { get; set; }
        /// <summary>
        /// Набор изображений для галерии данного бранда
        /// </summary>
        public virtual ICollection<Image> Images { get; set; }
    }
}
