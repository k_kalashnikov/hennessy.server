﻿using Hennessy.Core.Proto.Models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hennessy.Core.Proto.Models
{
    /// <summary>
    /// Модель сужности продукта
    /// </summary>
    public class Product : BaseModel<int>
    {
        /// <summary>
        /// Детальное описание продукта
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Процентное содержание спирта в данном продукте
        /// </summary>
        public decimal Spirits { get; set; }
        public int BrandId { get; set; }
        /// <summary>
        /// Бренд данного продукта
        /// </summary>
        public Brand Brand { get; set; }
        /// <summary>
        /// Набор ароматических свойств данного продукта
        /// </summary>
        public virtual ICollection<ProductAromatic> Aromatics { get; set; } 
        /// <summary>
        /// Набор объёмов, в которых выпускается данный продукт
        /// </summary>
        public virtual ICollection<ProductVolume> Volumes { get; set; }
        /// <summary>
        /// Рецепты данного продукта
        /// </summary>
        public virtual ICollection<ProductRecipe> Recipes { get; set; }
        public virtual ICollection<Image> Images { get; set; }
        public virtual ICollection<Property> Properties { get; set; }
        public virtual ICollection<ProductTag> Tags { get; set; }

    }
}
