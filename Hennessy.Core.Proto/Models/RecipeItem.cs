﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hennessy.Core.Proto.Models
{
    /// <summary>
    /// Модель описывающие сущность отдельно взятого инградиента для рецепта
    /// </summary>
    public class RecipeItem : BaseModel<int>
    {
        public decimal Count { get; set; }
        public int RecipeId { get; set; }
        public Recipe Recipe { get; set; }
    }
}
