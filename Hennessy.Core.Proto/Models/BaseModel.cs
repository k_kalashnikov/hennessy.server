﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Hennessy.Core.Proto.Models.Enum;

namespace Hennessy.Core.Proto.Models
{
    /// <summary>
    /// Базовая модель, которая содержит в себе свойства и поля, присущие всем другим сущностям (Кроме сущности ApplicationUser)
    /// </summary>
    public class BaseModel<TPK>
    {
        public BaseModel()
        {
            LastUpdate = DateTime.Now;
            Status = RowStatus.Normal;
            Name = "NONAME";
        }

        [Key]
        public TPK Id { get; set; }
        [Required]
        public string Name { get; set; }
        public DateTime LastUpdate { get; set; }
        public RowStatus Status { get; set; }
    }
}
