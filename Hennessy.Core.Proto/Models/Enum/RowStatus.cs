﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hennessy.Core.Proto.Models.Enum
{
    public enum RowStatus
    {
        Normal = 1,
        Delete = 0
    }
}
