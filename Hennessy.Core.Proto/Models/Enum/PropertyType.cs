﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hennessy.Core.Proto.Models.Enum
{
    public enum PropertyType
    {
        String = 0,
        Image = 1,
        Int = 2
    }
}
