﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hennessy.Core.Proto.Models.Enum
{
    public enum RecipeType
    {
        Cocktail = 0,
        Dish = 1
    }
}
