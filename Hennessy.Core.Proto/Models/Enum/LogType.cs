﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hennessy.Core.Proto.Models.Enum
{
    public enum LogType
    {
        Error = 0,
        Warning = 1,
        Success = 2
    }
}
