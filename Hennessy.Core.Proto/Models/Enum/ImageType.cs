﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hennessy.Core.Proto.Models.Enum
{
    public enum ImageType
    {
        BrandLogo = 0,
        BrandGallery = 1,
        ProductLogo = 2,
        ProductGallery = 3,
        RecipeLogo = 4,
        RecipeGallery = 5,
        AromaticIcon = 6
    }
}
