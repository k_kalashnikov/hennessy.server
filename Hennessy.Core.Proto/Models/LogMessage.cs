﻿using Hennessy.Core.Proto.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Hennessy.Core.Proto.Models
{
    public class LogMessage
    {
        public LogMessage()
        {
            Type = LogType.Success;
        }

#pragma warning disable CS1701 // Предполагается, что ссылка на сборку совпадает с удостоверением
        [Key]
#pragma warning restore CS1701 // Предполагается, что ссылка на сборку совпадает с удостоверением
        public int Id { get; set; }
        public int LogId { get; set; }
        public Log Log { get; set; }
        public string Message { get; set; }
        public LogType Type { get; set; }
    }
}
