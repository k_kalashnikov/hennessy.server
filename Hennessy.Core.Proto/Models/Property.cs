﻿using Hennessy.Core.Proto.Models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;

namespace Hennessy.Core.Proto.Models
{
    public class Property : BaseModel<int>
    {
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public string Value { get; set; }
        public PropertyType Type { get; set; }

        //public T getValueByType<T>(ApplicationDbContext _context = null) where T : class
        //{
        //    switch (this.Type)
        //    {
        //        case PropertyType.String:
        //            return Encoding.Unicode.GetString(Value) as T;
        //        case PropertyType.Image:
        //            return _context.Images.FirstOrDefault(m => m.Id == int.Parse(Encoding.Unicode.GetString(Value))) as T;
        //        case PropertyType.Int:
        //            return int.Parse(Encoding.Unicode.GetString(Value)) as T;

        //        default:
        //            return Value as T;
        //    }
        //}

        //public void setValueByType<T>(T val) where T : class
        //{
        //    switch (this.Type)
        //    {
        //        case PropertyType.Image:
        //        case PropertyType.Int:
        //        case PropertyType.String:
        //        default:
        //            Value = Encoding.Unicode.GetBytes(val.ToString());
        //            break;
        //    }
        //}
    }
}
