﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Hennessy.Core.Proto.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<ProductAromatic>().HasAlternateKey(m => new { m.AromaticId, m.ProductId });
            builder.Entity<ProductAromatic>().HasOne(m => m.Aromatic).WithMany(m => m.Products).HasForeignKey(m => m.AromaticId);
            builder.Entity<ProductAromatic>().HasOne(m => m.Product).WithMany(m => m.Aromatics).HasForeignKey(m => m.ProductId);

            builder.Entity<ProductVolume>().HasAlternateKey(m => new { m.ProductId, m.VolumeId });
            builder.Entity<ProductVolume>().HasOne(m => m.Volume).WithMany(m => m.Products).HasForeignKey(m => m.VolumeId);
            builder.Entity<ProductVolume>().HasOne(m => m.Product).WithMany(m => m.Volumes).HasForeignKey(m => m.ProductId);

            builder.Entity<ProductRecipe>().HasAlternateKey(m => new { m.ProductId, m.RecipeId });
            builder.Entity<ProductRecipe>().HasOne(m => m.Recipe).WithMany(m => m.Products).HasForeignKey(m => m.RecipeId);
            builder.Entity<ProductRecipe>().HasOne(m => m.Product).WithMany(m => m.Recipes).HasForeignKey(m => m.ProductId);

            builder.Entity<ProductTag>().HasAlternateKey(m => new { m.ProductId, m.TagId });
            builder.Entity<ProductTag>().HasOne(m => m.Product).WithMany(m => m.Tags).HasForeignKey(m => m.ProductId);
            builder.Entity<ProductTag>().HasOne(m => m.Tag).WithMany(m => m.Products).HasForeignKey(m => m.TagId);
        }

        #region Таблицы данных
        public DbSet<Brand> Brands { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<Recipe> Recipes { get; set; }
        public DbSet<RecipeItem> RecipeItems { get; set; }
        public DbSet<Aromatic> Aromatics { get; set; }
        public DbSet<Volume> Volumes { get; set; }
        public DbSet<Property> Properties { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<LogMessage> LogMessages { get; set; }
        #endregion

        #region Таблицы связей
        public DbSet<ProductAromatic> ProductAromatics { get; set; }
        public DbSet<ProductVolume> ProductVolumes { get; set; }
        #endregion
    }
}
