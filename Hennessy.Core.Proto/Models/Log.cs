﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Hennessy.Core.Proto.Models
{
    public class Log
    {
        public Log()
        {
            CreateFrom = DateTime.Now;
        }

        [Key]
        public int Id { get; set; }
        public virtual ICollection<LogMessage> Messages { get; set; }
        public DateTime CreateFrom { get; set; }
    }
}
