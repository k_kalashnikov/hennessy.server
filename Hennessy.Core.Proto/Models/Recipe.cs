﻿using Hennessy.Core.Proto.Models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hennessy.Core.Proto.Models
{
    /// <summary>
    /// Модель описывающие сущность рецепта
    /// </summary>
    public class Recipe : BaseModel<int>
    {
        /// <summary>
        /// Изображения для данного рецепта
        /// </summary>
        public virtual ICollection<Image> Images { get; set; }
        /// <summary>
        /// Набор ингредиентов для данного рецепта
        /// </summary>
        public virtual ICollection<RecipeItem> Items { get; set; }
        /// <summary>
        /// Инструкция по приготовлению
        /// </summary>
        public string Direction { get; set; }
        /// <summary>
        /// Детальное описание рецепта
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Продукты связанные с этим рецептом
        /// </summary>
        public ICollection<ProductRecipe> Products { get; set; }
        /// <summary>
        /// Тип рецепта(Блюдо или коктейль)
        /// </summary>
        public RecipeType Type { get; set; }
    }
}
