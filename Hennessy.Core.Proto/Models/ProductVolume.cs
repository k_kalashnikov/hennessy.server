﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hennessy.Core.Proto.Models
{
    public class ProductVolume : BaseModel<int>
    {
        public int ProductId { get; set; }
        public int VolumeId { get; set; }

        public virtual Product Product { get; set; }
        public virtual Volume Volume { get; set; }
    }
}
