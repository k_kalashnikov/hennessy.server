﻿using Hennessy.Core.Proto.Models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hennessy.Core.Proto.Models
{
    public class Image : BaseModel<int>
    {
        public string Checksum { get; set; }
        public ImageType Type { get; set; }

        public int? BrandId { get; set; }
        public int? ProductId { get; set; }
        public int? RecipeId { get; set; }
        public int? AromaticId { get; set; }

        public virtual Brand Brand { get; set; }
        public virtual Product Product { get; set; }
        public virtual Recipe Recipe { get; set; }
        public virtual Aromatic Aromatic { get; set; }
    }
}
