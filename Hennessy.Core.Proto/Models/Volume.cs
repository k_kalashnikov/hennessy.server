﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hennessy.Core.Proto.Models
{
    public class Volume : BaseModel<int>
    {
        public decimal Value { get; set; }
        public ICollection<ProductVolume> Products { get; set; }
    }
}
