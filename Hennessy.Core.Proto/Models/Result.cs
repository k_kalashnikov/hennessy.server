﻿using Hennessy.Core.Proto.Models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hennessy.Core.Proto.Models
{
    public class Result<TModel> where TModel : class
    {
        public LogMessage Message { get; set; }
        public TModel Object { get; set; }
        public bool Status { get; set; }

        public Result(TModel _Object, string _Message = "", LogType _LogType = LogType.Success, bool _Status = true)
        {
            Message = new LogMessage()
            {
                Message = _Message,
                Type = _LogType
            };
            Object = _Object;
            Status = _Status;
        }

    }
}
