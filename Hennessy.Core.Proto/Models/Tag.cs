﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hennessy.Core.Proto.Models
{
    public class Tag : BaseModel<int>
    {
        public ICollection<ProductTag> Products { get; set; }
    }
}
